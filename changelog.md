### 2.5.0

* Suppression de tous les logs de Vlad sauf les plus essentiels du player (il passe à la version 1.16.5).
* log2ram actif dès le début.
* Suppression du reboot journalier, à sa place le Pi exécute le script de boot et un autre qui réinitialise les timers de fetcher et downloader, et qui redémarre le Pi seulement le dimanche (bientôt on fera une fois par mois).
* Les logs du système tournent chaque jour (avant une fois par semaine).
* Le fetcher musique s'exécute toutes les trois heures (par toutes les deux jusqu'à maintenant).

### 2.4.0

* Suppression du service first boot.
* Fetcher de musique toutes les deux heures (avant ça toutes les heures).
* Ajout d'un log au coming files (service de télechargement en amont).
* Deuxième version du process monitor, maintenant il détecte aussi quand le controller n'existe pas.
* Le normalisateur de fichiers mp3 fait seulement la musique.
* On remet le timer vlad-volume-control, car il s'occupe aussi des modulations horaires.

### 2.3.0

* Service vlad-coming_files, qui télécharge tous les fichiers programmées pour l'avenir (sauf playlist perso). S'éxécute par un timer tous les jours entre 19:30 et 20:30.
* Inclut un petit script sans lequel le bouton 'Channel history' du dashboard ne retourne rien. Ce bouton liste les changements de chaine du player pendant la journée.
* Le timer vlad-volume-control disparaît, et à sa place ce service s'éxécute à chaque reboot et quand on change la valeur du Volume par défaut au dashboard.
* Fix normalize files

### 2.2.0

* Service player-process-monitor, qui toutes les cinq minutes vérifie les procès du player, et fait un restart automatique si le Pi reste sans annonces ou musique.
* Service channel_timer_control, qui chaque jour à 7 heures synchronise les timers avec les chaines actives au dashboard. Il s'éxécute aussi à chaque appui d'un bouton "ACTIVE CHANNELS" au dashboard (1).
* Service audio_file_cleanup, qui tous les dimanches nettoye les fichiers mp3 plus anciens (un an lors de la dernière apparition dans la playlist pour la musique, une semaine pour les annonces, et aussi les fichiers qui n'existent pas dans la base de données).
* Le patch_downloader du player (responsable du last_seen) fait un suivi de la température du Pi (2).
* Les fetchers de musique et annonces passent à une fois par heure (avant c'était 20 et 10 minutes respectivement).
* Corrigé un bug au fetcher flash (timeout était au timer et pas au service comme il devrait)

1) Le player passe à la version 1.6.3
2) Le player passe à la version 1.6.4
Voir changelog du player ici: https://github.com/Audiadis/Audiabox_Player/blob/track_temperature/changelog.MD

### 2.1.6

* Identique à la 2.2.0 sauf qu'il manque le service audio_file_cleanup, et au reboot on active le wifi après le check LAN.

### 2.1.5

* Nouveau système pour que le Pi soit toujours à la bonne heure, on active NTP ou non au dashboard, selon si le réseau du magasin le permet.
* Nouveau endpoint pour les appels téléphoniques des bornes (le player passe de la version 1.16.1 à la 1.16.2)

### 2.1.3

* Le tunnel ne s'ouvre plus automatiquement, on indique au dashboard si le Pi doit l'ouvrir ou non.

### 2.1.2

* Le Pi se met à la bonne date avant d'essayer de se connecter à notre serveur. Souci provoqué par le nouveau certificat SSL

### 2.1.0

* Nouvelles clefs SSH et nouveau mot de passe pour l'utilisateur Pi

### 2.0.2

* Possibilité de désactiver la config de LAN dynamique au script de démarrage.

### 2.0.1

* Raspbian 10
* Fix carte son externe
